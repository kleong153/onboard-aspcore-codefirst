﻿using System;
using Code_First.Repository.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Code_First.Repository
{
    public class CDBContext : IdentityDbContext<ApplicationUser>
    {
        public CDBContext(DbContextOptions<CDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<News>()
                .HasIndex(p => new { p.Title, p.Status, p.CreatedDate });

            builder.Entity<NewsCategory>()
                .HasIndex(p => new { p.Name, p.Status });
        }

        public DbSet<News> News { get; set; }

        public DbSet<NewsCategory> NewsCategory { get; set; }
    }
}
