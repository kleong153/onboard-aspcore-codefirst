﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Code_First.Repository.Models
{
    [Table("News")]
    public class News
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "BIGINT")]
        public long Id { get; set; }

        [MaxLength(300)]
        public string Title { get; set; }

        public string Description { get; set; }

        public string URL { get; set; }

        public long NewsCategoryId { get; set; }

        [ForeignKey("NewsCategoryId")]
        public virtual NewsCategory NewsCategory { get; set; }

        public CommonStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatorName { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }
    }
}
