﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Code_First.Repository.Models
{
    [Table("NewsCategory")]
    public class NewsCategory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "BIGINT")]
        public long Id { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }

        public CommonStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public virtual ICollection<News> News { get; set; }
    }
}
