﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Code_First.Repository.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
