﻿using System;
namespace Code_First.Repository.Models
{
    public enum CommonStatus
    {
        Deleted = 0,
        Active = 1
    }
}
