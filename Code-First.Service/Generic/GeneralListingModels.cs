﻿using System;
using System.Collections.Generic;

namespace Code_First.Service.Models
{
    public class GeneralListingRequestModel
    {
        public string OrderDir { get; set; }

        public string SortColumn { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string SearchKeyword { get; set; }
    }

    public class GeneralListingResponseModel<T>
    {
        public List<T> List { get; set; }

        public int TotalCount { get; set; }
    }
}
