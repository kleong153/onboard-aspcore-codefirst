﻿using System;
namespace Code_First.Service.Models
{
    public class GenericRequestModel
    {
        public long Id { get; set; }
    }

    public class GenericResponseModel
    {
        public bool Success { get; set; }

        public string Message { get; set; }
    }

    public class GeneralMasterDataModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int SortOrder { get; set; }
    }
}