﻿using System;
using System.Linq;
using System.Collections.Generic;
using Code_First.Repository;
using Code_First.Service.Models;

namespace Code_First.Service
{
    public class NewsCategoryService
    {
        private readonly CDBContext _cdbContext;

        public NewsCategoryService(CDBContext cdbContext)
        {
            _cdbContext = cdbContext;
        }

        public GeneralListingResponseModel<NewsCategoryModel> GetNewsCategories(GeneralListingRequestModel model)
        {
            #region null-handle
            if (string.IsNullOrEmpty(model.OrderDir))
            {
                model.OrderDir = "asc";
            }
            else
            {
                if (model.OrderDir.ToLower().Trim() != "asc" && model.OrderDir.ToLower().Trim() != "desc")
                {
                    model.OrderDir = "asc";
                }
            }
            if (model.PageSize == 0)
            {
                model.PageSize = 10;
            }
            if (string.IsNullOrEmpty(model.SortColumn))
            {
                model.SortColumn = "";
            }
            #endregion

            var query = from e in _cdbContext.NewsCategory
                        where e.Status == Repository.Models.CommonStatus.Active
                        where (string.IsNullOrEmpty(model.SearchKeyword) || e.Name.Contains(model.SearchKeyword))
                        select new NewsCategoryModel()
                        {
                            NewsCategoryId = e.Id,
                            Name = e.Name
                        };

            switch (model.OrderDir.Trim().ToLower())
            {
                case "asc":
                    switch (model.SortColumn.ToLower())
                    {
                        case "name":
                            query = query.OrderBy(e => e.Name);
                            break;
                        default:
                            query = query.OrderBy(e => e.NewsCategoryId);
                            break;
                    }
                    break;
                case "desc":
                    switch (model.SortColumn.ToLower())
                    {
                        case "name":
                            query = query.OrderByDescending(e => e.Name);
                            break;
                        default:
                            query = query.OrderByDescending(e => e.NewsCategoryId);
                            break;
                    }
                    break;
            }

            var response = new GeneralListingResponseModel<NewsCategoryModel>()
            {
                TotalCount = query.Count(),
                List = query.Skip(model.PageIndex * model.PageSize).Take(model.PageSize).ToList()
            };

            return response;
        }

        public List<GeneralMasterDataModel> GetCategoryMultiDropdownList(string searchKeyword, int skip = 0, int take = 50)
        {
            var query = from e in _cdbContext.NewsCategory
                        where (string.IsNullOrEmpty(searchKeyword) || e.Name.Contains(searchKeyword))
                        where e.Status == Repository.Models.CommonStatus.Active
                        orderby e.Name
                        select new GeneralMasterDataModel()
                        {
                            Id = e.Id,
                            Name = e.Name
                        };

            return query.Skip(skip).Take(take).ToList();
        }
    }
}
