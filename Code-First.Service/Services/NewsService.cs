﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Code_First.Repository;
using Code_First.Service.Models;
using Microsoft.EntityFrameworkCore;

namespace Code_First.Service
{
    public class NewsService
    {
        private readonly CDBContext _cdbContext;

        public NewsService(CDBContext cdbContext)
        {
            _cdbContext = cdbContext;
        }

        public GeneralListingResponseModel<NewsModel> GetNewsList(GeneralListingRequestModel model)
        {
            return GetNewsListAsync(model).Result;
        }

        public async Task<GeneralListingResponseModel<NewsModel>> GetNewsListAsync(GeneralListingRequestModel model)
        {
            #region null-handle
            if (string.IsNullOrEmpty(model.OrderDir))
            {
                model.OrderDir = "asc";
            }
            else
            {
                if (model.OrderDir.ToLower().Trim() != "asc" && model.OrderDir.ToLower().Trim() != "desc")
                {
                    model.OrderDir = "asc";
                }
            }
            if (model.PageSize == 0)
            {
                model.PageSize = 10;
            }
            if (string.IsNullOrEmpty(model.SortColumn))
            {
                model.SortColumn = "";
            }
            #endregion

            var query = from e in _cdbContext.News
                        where e.Status == Repository.Models.CommonStatus.Active
                        where (string.IsNullOrEmpty(model.SearchKeyword) || e.Title.Contains(model.SearchKeyword))
                        select new NewsModel()
                        {
                            NewsId = e.Id,
                            Title = e.Title,
                            URL = e.URL,
                            CreatedDate = e.CreatedDate
                        };

            switch (model.OrderDir.Trim().ToLower())
            {
                case "asc":
                    switch (model.SortColumn.ToLower())
                    {
                        case "title":
                            query = query.OrderBy(e => e.Title);
                            break;
                        default:
                            query = query.OrderBy(e => e.CreatedDate);
                            break;
                    }
                    break;
                case "desc":
                    switch (model.SortColumn.ToLower())
                    {
                        case "title":
                            query = query.OrderByDescending(e => e.Title);
                            break;
                        default:
                            query = query.OrderByDescending(e => e.CreatedDate);
                            break;
                    }
                    break;
            }

            var response = new GeneralListingResponseModel<NewsModel>()
            {
                TotalCount = query.Count(),
                List = await query.Skip(model.PageIndex * model.PageSize).Take(model.PageSize).ToListAsync()
            };

            return response;
        }

        public NewsModel GetNews(long NewsId)
        {
            #region null-handle
            if (NewsId == 0)
            {
                throw new Exception("No Id specified");
            }
            #endregion

            var entityNews = _cdbContext.News.Where(e => e.Id == NewsId && e.Status == Repository.Models.CommonStatus.Active).FirstOrDefault();

            if (entityNews == null)
            {
                throw new Exception("Record not found.");
            }

            return new NewsModel(entityNews);
        }

        public GenericResponseModel SubmitNews(NewsModel model)
        {
            var result = new GenericResponseModel();

            if (model.NewsId == 0)
            {
                #region Add New News

                #region null-handle

                #endregion

                #region Create

                #endregion

                #endregion
            }
            else
            {
                #region Edit News

                #region null-handle
                if (string.IsNullOrEmpty(model.Title))
                {
                    throw new Exception("Title is required");
                }
                if (string.IsNullOrEmpty(model.Description))
                {
                    throw new Exception("Description is required");
                }
                if (model.NewsCategory == null)
                {
                    throw new Exception("News category is required");
                }

                var entityNews = _cdbContext.News.Where(e => e.Id == model.NewsId && e.Status == Repository.Models.CommonStatus.Active).FirstOrDefault();
                if (entityNews == null)
                {
                    throw new Exception("Record not found");
                }
                #endregion

                #region Update News
                bool hasUpdate = false;
                if (model.Title != entityNews.Title)
                {
                    entityNews.Title = model.Title;
                    hasUpdate = true;
                }
                if (model.Description != entityNews.Description)
                {
                    entityNews.Description = model.Description;
                    hasUpdate = true;
                }
                if (model.URL != entityNews.URL)
                {
                    entityNews.URL = model.URL;
                    hasUpdate = true;
                }
                if (model.CreatorName != entityNews.CreatorName)
                {
                    entityNews.CreatorName = model.CreatorName;
                    hasUpdate = true;
                }
                if (model.NewsCategory.NewsCategoryId != entityNews.NewsCategoryId)
                {
                    entityNews.NewsCategoryId = model.NewsCategory.NewsCategoryId;
                    hasUpdate = true;
                }

                if (hasUpdate)
                {
                    entityNews.UpdatedBy = model.CreatorName;
                    entityNews.UpdatedDate = DateTime.UtcNow;

                    _cdbContext.SaveChanges();

                    result = new GenericResponseModel()
                    {
                        Success = true
                    };
                }
                #endregion

                #endregion
            }

            return result;
        }
    }
}
