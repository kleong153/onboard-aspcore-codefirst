﻿using System;
namespace Code_First.Service.Models
{
    public class NewsCategoryModel
    {
        public long NewsCategoryId { get; set; }

        public string Name { get; set; }
    }
}
