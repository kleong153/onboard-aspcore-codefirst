﻿using System;
using Code_First.Repository.Models;

namespace Code_First.Service.Models
{
    public class NewsModel
    {
        public long NewsId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string URL { get; set; }

        public CommonStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string CreatorName { get; set; }

        public NewsCategoryModel NewsCategory { get; set; }

        public NewsModel()
        {
            NewsCategory = new NewsCategoryModel();
        }

        public NewsModel(News entityNews)
        {
            NewsId = entityNews.Id;
            Title = entityNews.Title;
            Description = entityNews.Description;
            URL = entityNews.URL;
            CreatedDate = entityNews.CreatedDate;
            CreatorName = entityNews.CreatorName;
            NewsCategory = new NewsCategoryModel();
            NewsCategory.NewsCategoryId = entityNews.NewsCategoryId;
            NewsCategory.Name = entityNews.NewsCategory.Name;
        }
    }
}
