﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code_First.Repository;
using Code_First.Service;
using Code_First.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Code_First.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NewsCategoryController : ControllerBase
    {
        private readonly CDBContext _context;
        private readonly NewsCategoryService _newsCategoryService;

        public NewsCategoryController(CDBContext context)
        {
            _context = context;
            _newsCategoryService = new Service.NewsCategoryService(_context);
        }

        [HttpPost("v1/getNewsCategories")]
        public ActionResult<GeneralListingResponseModel<NewsCategoryModel>> GetNewsCategory(GeneralListingRequestModel model)
        {
            return _newsCategoryService.GetNewsCategories(model);
        }

        [HttpGet("v1/getCategoryMultiDropdownList")]
        public ActionResult<List<GeneralMasterDataModel>> GetCategoryMultiDropdownList(string searchKeyword, int skip = 0, int take = 50)
        {
            return _newsCategoryService.GetCategoryMultiDropdownList(searchKeyword, skip, take);
        }
    }
}
