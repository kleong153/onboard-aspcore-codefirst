﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code_First.Repository;
using Code_First.Service;
using Code_First.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Code_First.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NewsController : ControllerBase
    {
        private readonly CDBContext _context;
        private readonly NewsService _newsCategoryService;

        public NewsController(CDBContext context)
        {
            _context = context;
            _newsCategoryService = new Service.NewsService(_context);
        }

        [HttpPost("v1/getNewsList")]
        public ActionResult<GeneralListingResponseModel<NewsModel>> GetNewsCategory(GeneralListingRequestModel model)
        {
            return _newsCategoryService.GetNewsList(model);
        }

        [HttpGet("v1/getNews")]
        public ActionResult<NewsModel> GetNews(long newsId)
        {
            return _newsCategoryService.GetNews(newsId);
        }

        [HttpPost("v1/submitNews")]
        public ActionResult<GenericResponseModel> GetCategoryMultiDropdownList(NewsModel model)
        {
            return _newsCategoryService.SubmitNews(model);
        }
    }
}
